package com.demo.service2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.lang.invoke.MethodHandles;

@SpringBootApplication
@RestController
public class Service2Application {

	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	@Autowired
	RestTemplate restTemplate;
	@Value("${service3.address:localhost:8082}") String service3;

	public static void main(String[] args) {
		SpringApplication.run(Service2Application.class, args);
	}
	@RequestMapping("/")
	public String frontPage() throws InterruptedException {
		log.info("Front Page");
		return "Front Page";
	}

	@RequestMapping("/startOfService2")
	public String startOfService2() throws InterruptedException {
		log.info("Welcome To Service2. Calling Service3");
		//String response = restTemplate.getForObject("http://" + service3 + "/startOfService3", String.class);
		//hardcoding for testing, remove after
		String response = restTemplate.getForObject("https://gitlab-microservices-demo-service3.34.66.106.66.nip.io/startOfService3", String.class);
		log.info("Got response from service3 [{}]", response);
		return response;
	}



	@Bean
	RestTemplate restTemplate() {
		return new RestTemplate();
	}

}
